import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.*;
import java.net.*;
import java.util.*;

public class Client {
    public static void main(String[] args) {
        Socket socket;//套接字对象
        DataInputStream in=null;//输入流
        DataOutputStream out=null;//输出流
        //中缀表达式的输入
        String str;
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入中缀表达式：");
        str=scanner.nextLine();
        try {
            socket=new Socket("localhost",5203);
            in=new DataInputStream(socket.getInputStream());
            out=new DataOutputStream(socket.getOutputStream());
            out.writeUTF(str);
            String s=in.readUTF();
            System.out.println("结果为：\n"+s);
        }
        catch (Exception e) {
            System.out.println("服务器已断开"+e);
        }
    }
}
