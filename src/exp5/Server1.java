import java.io.*;
import java.net.*;
public class Server1 {
    public static void main(String args[]) {
        String key="";
        int n=-1;
        byte [] a=new byte[128];
        try{  File f=new File("key1.dat");
            InputStream in = new FileInputStream(f);
            while((n=in.read(a,0,100))!=-1) {
                key=key+new String (a,0,n);
            }
            in.close();
        }
        catch(IOException e) {
            System.out.println("File read Error"+e);
        }
        ServerSocket serverForClient=null;
        Socket socketOnServer=null;
        DataOutputStream out=null;
        DataInputStream  in=null;
        try { serverForClient = new ServerSocket(5006);
        }
        catch(IOException e1) {
            System.out.println(e1);
        }
        try{ System.out.println("等待客户呼叫");
            socketOnServer = serverForClient.accept(); //堵塞状态，除非有客户呼叫
            out=new DataOutputStream(socketOnServer.getOutputStream());
            in=new DataInputStream(socketOnServer.getInputStream());
            String s=in.readUTF(); // in读取信息，堵塞状态
            String ming=EncryptDecrypt.AESDncode(key,s);
            MyDC mydc=new MyDC();
            int answer=mydc.evaluate(ming);
            System.out.println("服务器收到的信息为:"+s);
            System.out.println("服务器解密后明文为:"+ming);
            System.out.println("服务器计算出的答案为:"+answer);
            out.writeUTF(answer+"");
            Thread.sleep(500);
        }
        catch(Exception e) {
            System.out.println("客户已断开"+e);
        }
    }
}


