import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex c1 = new Complex(2,1);
    Complex c2 = new Complex(0,3);
    Complex c3 = new Complex(-1,-1);
    @Test
    public void testgetRealPart() throws Exception {
        assertEquals(0.0,Complex.getRealPart(0.0));
        assertEquals(-2.0,Complex.getRealPart(-2.0));
        assertEquals(6.0,Complex.getRealPart(6.0));
    }
    @Test
    public void testComplexAdd() throws Exception{
        assertEquals("2.0+4.0i",c1.ComplexAdd(c2).toString());
        assertEquals("-1.0+2.0i",c2.ComplexAdd(c3).toString());
        assertEquals("1.0",c1.ComplexAdd(c3).toString());
    }
    @Test
    public void testComplexSub() throws Exception{
        assertEquals("2.0 -2.0i",c1.ComplexSub(c2).toString());
        assertEquals("1.0+4.0i",c2.ComplexSub(c3).toString());
        assertEquals("-3.0 -2.0i",c3.ComplexSub(c1).toString());
    }
    @Test
    public void testComplexMulti() throws Exception {
        assertEquals("-3.0+6.0i",c1.ComplexMulti(c2).toString());
        assertEquals("3.0 -3.0i",c2.ComplexMulti(c3).toString());
        assertEquals("-1.0 -3.0i",c3.ComplexMulti(c1).toString());
    }
    @Test
    public void testComplexDiv() throws Exception {
        assertEquals("0.6666666666666666+0.3333333333333333i",c1.ComplexDiv(c2).toString());
        assertEquals("-1.5 -1.5i",c2.ComplexDiv(c3).toString());
        assertEquals("-0.6 -0.6i",c3.ComplexDiv(c1).toString());
    }

}
