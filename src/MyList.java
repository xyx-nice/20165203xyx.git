import java.util.*;
public class MyList {
    public static void main(String [] args) {
        List<String> list=new LinkedList<String>();
        list.add("20165201");
        list.add("20165202");
        list.add("20165204");
        list.add("20165205");
        System.out.println("打印初始链表");
        //把上面四个节点连成一个没有头结点的单链表
        Iterator<String> iter=list.iterator();
        while(iter.hasNext()){
            String te=iter.next();
            System.out.println(te);
        }
        //遍历单链表，打印每个结点的
        list.add("20165203");
        //把你自己插入到合适的位置（学号升序）
        System.out.println("插入我的学号后排序，打印链表");
        Collections.sort(list);
        iter=list.iterator();
        while(iter.hasNext()){
            String te=iter.next();
            System.out.println(te);
        }
        //遍历单链表，打印每个结点的
        list.remove("20165203");
        //从链表中删除自己
        System.out.println("删除我的学号后打印链表");
        iter=list.iterator();
        while(iter.hasNext()){
            String te=iter.next();
            System.out.println(te);
        }
        //遍历单链表，打印每个结点的
    }
}

