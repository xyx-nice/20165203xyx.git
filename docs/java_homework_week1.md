#  20165203 2017-2018-2《Java程序设计》第一周学习总结

## 教材学习内容总结
#### （一）Java的地位  

Java是面向对象编程，并涉及网络、多线程等重要的基础知识，是一门很好的面向对象的语言。  

#### （二）Java的特点  

简单 面向对象 平台无关 多线程 动态   

#### （三）Java三大平台  

Java SE    Java EE    Java ME  

####  (四)Java SE的四个组成部分 
 
 JVM: Java虚拟机，包含在JRE中  
 JRE：Java执行环境，运行Java程序必需  
 JDK: 包括JRK及开发过程中需要的工具，如javac、java等工具程序，开发Java程序必需  
 Java语言  
 
  **所以，若只执行Java程序，不下载JDK也是可以的  ** 
 
####  （五）安装JDK  

##### 1.Windows上安装JDK  

根据教材中的的方法，先登录官网···http://www.oracle.com/technetwork/java/javase/downloads/index.html···， 安装成功后如下图所示
![JDK安装成功](https://gitee.com/uploads/images/2018/0304/210518_fe2b9171_1744920.jpeg "安装成功JDK.JPG")
，注意，JDK和JRE的路径不可以相同，因为JDK本身已经包含JRE。  

#####  2.系统环境变量设置   

设置系统变量Java_home,变量值设置成jdk1.8的路径
![Java_home](https://gitee.com/uploads/images/2018/0304/210611_74573bd9_1744920.jpeg "JAVAHOME.JPG")  

##### 3.系统环境Path的设置  

编辑Path,添加jdk1.8的路径  
并添加%JAVA_HOME%\bin 
![path](https://gitee.com/uploads/images/2018/0304/210652_1dade785_1744920.jpeg "PATH1.JPG") 

#### (六) Java程序的开发步骤 

##### 1.在记事本中编写源文件：  
- 源文件的扩展名必须是.java
- 源文件的名字必须是某个类的名字  
- 如果源文件中有public类，那么源文件的名字必须是这个类的名字  
- 源文件至多有1个public类  
 
##### 2.编译源文件 

javac 文件名.java  

##### 3.运行  

- 运行主类（有void man)
- java <主类名>, 不加扩展名  
 
##### 4.反编译  

反编译类文件
- javap Hello.class

---

## 学习中遇到的问题及解决  


Q：之前设置好环境变量时，在命令行中输入javac时，出现错误提示，显示javac并不是外部命令，之前，环境变量都是按照教材提示来设置，所以很头疼。  

A：在同学的帮助下，发现是我没有在环境变量path中加入jdk1.8的地址。如图所示， 
![path](https://gitee.com/uploads/images/2018/0304/210716_1b0efc71_1744920.jpeg "PATH2.JPG") 

git 设置中出现的问题  

Q：输入git remote add origin http://git.oschina.net/用户名/项目名.git时出现拼写错误
A：输入 git pull origin master,然后退出编辑页面，再重新输入git push origin master.

Q：在虚拟机中创建一个文件，试图用cd查看，但是，无济于事 
A：cd用来切换至所要到达的文件目录下，ls才是查看文件  

Q：自己设置好run.sh的脚本文件后，显示失败，不知道是什么原因？  
A：自己查教程后发现，没有加入
```
chmod +x run.sh
```
的设置权限功能。 设置成功后如下图所示  
![rush](https://gitee.com/uploads/images/2018/0304/210753_164efd95_1744920.jpeg "run.sh脚本.JPG")


Q:在自己使用vim编辑器输入代码时，在普通模式下如何移动光标？
A：经过查阅百度，发现使用 
```
graph LR
H-->左
```

```
graph LR
J-->下
```

```
graph LR
K-->上
```

```
graph LR
L-->右
```
--[vim下如何移动光标](https://www.cnblogs.com/caochuangui/p/5851083.html)  

Q：在设置git时，出现一些设置不成功问题。  
A：总结问题解决方案如下：
- 自己可能代码敲错
- 权限未设置
- git命令输错，要及时改正
在同学的帮助下，我成功将代码上传，如图所示
![上传的代码](https://gitee.com/uploads/images/2018/0304/213027_c766ab68_1744920.jpeg "提交.JPG")


---
## 代码调试中的问题及解决过程  

存在一些字母大小写搞错问题，并已妥善解决，目前代码较为简单，还未出现大问题。  

---
## [代码托管](https://gitee.com/xyx-nice/20165203xyx.git)
---
## 上周考试错题总结 

如果只是要运行Java程序，下载程序哪个安装即可？  

A.JDK  B.JRE  C.JavaDoc  D.Glassfish

答案：B
解析：编译程序JDK  
      下载程序JRE


---
## 其他  

本周学习，第一周要学习很多提交作业的方法还要零基础学Java,给我的感受是遇到不懂的概念一定不要着急，也不要盲目依赖他人，先尝试自己解决问题（百度或查阅其他资料都可以），发现自己解决不了，再和他人探讨，并且要有所领悟。在过去的一周里，真的要感谢帮助过我的同学们，学习Java之路我从不孤单，因为有你们，有大家的帮助，大家齐心协力，一定可以攻克Java的难关，取得理想的成绩。  


---
